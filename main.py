#!/usr/bin/env python3

import logging

from ROT1Coder import ROT1Coder
from ConfigException import ConfigException
from MyConfigReader import MyConfigReader
from MyFileReader import MyFileReader

class MainClass:
    def __init__(self):
        self._config_reader: MyConfigReader = MyConfigReader()
        self._file_reader: MyFileReader = MyFileReader()
        self._coder: ROT1Coder = ROT1Coder()

    def run(self, config_file_name: str):
        try:
            configuration = self._config_reader.read_config(config_file_name)
            file_name = configuration[self._config_reader.file_name_for_coder_param_name]
            coder_configuration = configuration[self._config_reader.coder_run_option_param_name]
            
            try:
                buffer_size = int(configuration[self._config_reader.buffer_size_param_name])
            except:
                raise ConfigException('Неверное значение buffer_size')

            if buffer_size <= 0 or isinstance(buffer_size, float):
                raise ConfigException('Неверное значение buffer_size')

            with self._file_reader.read_file(file_name, buffer_size) as chunks:
                for chunk in chunks:
                    chunk_res = self._coder.run(coder_configuration, str(chunk))
                    print(chunk_res, end='')
        except ConfigException as e:
            logging.error(e)
            return ""

if __name__ == '__main__':
    argv_config_file_name = input()
    main_class = MainClass()
    main_class.run(argv_config_file_name)
