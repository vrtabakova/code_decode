from CoderInterface import CoderInterface

class ROT1Coder(CoderInterface):
  def run(self, coder_info, string_to_process):
    if coder_info == 'code':
      return self._code(string_to_process)
    if coder_info == 'decode':
      return self._decode(string_to_process)
    raise ValueError(f'Опция {coder_info} не поддерживается')

  def _code(self, string_to_code):
    result = ''

    for v in string_to_code:
      c = ord(v)
      if c >= ord('a') and c <= ord('z'):
        result += chr((c - ord('a') + 1) % 26 + ord('a'))
      elif c >= ord('A') and c <= ord('Z'):
        result += chr((c - ord('A') + 1) % 26 + ord('A'))
      else:
        result += v

    return result
  

  def _decode(self, string_to_code):
    result = ''

    for v in string_to_code:
      c = ord(v)
      if c >= ord('a') and c <= ord('z'):
        result += chr((c - ord('a') - 1) % 26 + ord('a'))
      elif c >= ord('A') and c <= ord('Z'):
        result += chr((c - ord('A') - 1) % 26 + ord('A'))
      else:
        result += v

    return result