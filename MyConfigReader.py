from ConfigReaderInterface import ConfigReaderInterface
from ConfigException import ConfigException

class MyConfigReader(ConfigReaderInterface):
  def read_config(self, file_path):
    config = {}

    try:
      with open(file_path, 'r') as file:
        for line in file:
          key, value = line.strip().split('=', 1)
          config[key.strip()] = value.strip()
      return config
    except Exception as e:
      raise ConfigException(f'Ошибка в конфигурационном файле: {str(e)}')
